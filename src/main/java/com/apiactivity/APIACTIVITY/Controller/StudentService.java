package com.apiactivity.APIACTIVITY.Controller;

import com.apiactivity.APIACTIVITY.Entity.Student;
import com.apiactivity.APIACTIVITY.Repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    private final StudentRepository studentRepository;

    @Autowired

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    //findall
    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }
    //find by id
    public Optional<Student> getStudentById(Long id) {
        return studentRepository.findById(id);
    }
    //Post
    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }
    //delete
    public void deleteStudentById(Long id) {
        studentRepository.deleteById(id);
    }
    //update
    public Student updateStudent(Student updatedStudent) {
        return studentRepository.save(updatedStudent);
    }


}
