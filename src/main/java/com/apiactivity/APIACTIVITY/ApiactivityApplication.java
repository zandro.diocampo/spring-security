package com.apiactivity.APIACTIVITY;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiactivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiactivityApplication.class, args);
	}

}
