package com.apiactivity.APIACTIVITY.Repository;

import com.apiactivity.APIACTIVITY.Entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
